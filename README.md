# First Django Project ToDos



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/learning-projects144720/first-django-project-todos.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/learning-projects144720/first-django-project-todos/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
My first Django Project based upon the following tutorial;
[Learn Django in 20 Minutes!](https://youtu.be/nGIg40xs9e4?si=ASMGjpzBP5IPtAxN)

## Description
This project utilizes Python and the starter package and web templates for Django.
It is the first Django project that I have attempted that actually works.

## Badges
No unit tests or other badges are included.

## Visuals
N/A

## Installation
There is no installation just the base PyCharm files you can use to create your own.

## Usage
Set to run locally on your machine and convey base Django utility

## Support
For additional help please refer to the weblink above on YouTube.
Channel Name is 'Tech With Tim'.

## Roadmap
Stil learning so I'm not 100% sure where to go with this.
I'm attempting his full course [Django For Beginners - Full Tutorial](https://youtu.be/sm1mokevMWk?si=TH1TueORDUJIuYme)

## Contributing
Since this is a starter project I'm not anticipating any contributions, just connecting my projects from PyCharm to my GitLab repository.

## Authors and acknowledgment
I want to acknowledge 'Tech With Tim' for this project.

## License
N/A

## Project status
Project status is complete, however I may return and build this out even further.
